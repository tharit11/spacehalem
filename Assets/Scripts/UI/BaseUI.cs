﻿using UnityEngine;

namespace UI
{
    public abstract class BaseUI : MonoBehaviour, IVisible
    {
        public bool IsActive => gameObject.activeSelf;

        public virtual void Awake()
        {
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
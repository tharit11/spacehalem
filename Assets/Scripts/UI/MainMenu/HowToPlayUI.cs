﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.MainMenu
{
    public class HowToPlayUI : BaseUI
    {
        [SerializeField] private Button closeButton;

        public override void Awake()
        {
            base.Awake();

            Debug.Assert(closeButton != null, "closeButton can't be null!");
            closeButton.onClick.AddListener(Hide);

            Hide();
        }
    }
}
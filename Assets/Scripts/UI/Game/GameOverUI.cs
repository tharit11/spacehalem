﻿using Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Game
{
    public class GameOverUI : BaseUI
    {
        [SerializeField] private TextMeshProUGUI summaryText;
        [SerializeField] private Button retryButton;
        [SerializeField] private Button leaveButton;

        public override void Awake()
        {
            base.Awake();

            Debug.Assert(summaryText != null, "summaryText can't be null!");
            Debug.Assert(retryButton != null, "retryButton can't be null!");
            Debug.Assert(leaveButton != null, "leaveButton can't be null!");

            retryButton.onClick.AddListener(() => { LevelManager.LoadLevel(LevelManager.Type.Game); });
            leaveButton.onClick.AddListener(() => { LevelManager.LoadLevel(LevelManager.Type.MainMenu); });

            Hide();
        }

        public void SetScore(int totalScore)
        {
            summaryText.text = $"Score: {totalScore}";
        }
    }
}
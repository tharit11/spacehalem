﻿using TMPro;
using UnityEngine;

namespace UI.Game
{
    public class GameUI : BaseUI
    {
        [SerializeField] private HealthBarUI healthBarUI;
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI magCountText;
        [SerializeField] private TextMeshProUGUI ammoCountText;
        [SerializeField] private TextMeshProUGUI enemyCountText;
        [SerializeField] private TextMeshProUGUI cartridgeCountText;

        public override void Awake()
        {
            base.Awake();

            Debug.Assert(healthBarUI != null, "healthBarUI can't be null!");
            Debug.Assert(scoreText != null, "scoreText can't be null!");
            Debug.Assert(magCountText != null, "magCountText can't be null!");
            Debug.Assert(ammoCountText != null, "ammoCountText can't be null!");
            Debug.Assert(enemyCountText != null, "enemyCountText can't be null!");
            Debug.Assert(cartridgeCountText != null, "cartridgeCountText can't be null!");
        }

        public void SetScore(int newScore)
        {
            scoreText.text = $"{newScore:N0}";
        }

        public void SetHealth(int newHealth)
        {
            healthBarUI.SetHealthPoint(newHealth);
        }

        public void SetMagCount(int value)
        {
            magCountText.text = $"{value}";
        }

        public void SetAmmoCount(int value)
        {
            ammoCountText.text = $"{value}";
        }

        public void SetEnemyCount(int value)
        {
            enemyCountText.text = $"{value}";
        }

        public void SetCartridgeCount(int value)
        {
            cartridgeCountText.text = $"{Mathf.Clamp(value, 0, int.MaxValue)}";
        }
    }
}
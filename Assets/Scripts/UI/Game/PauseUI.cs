﻿using Manager;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Game
{
    public class PauseUI : BaseUI
    {
        [SerializeField] private Button resumeButton;
        [SerializeField] private Button leaveButton;

        public override void Awake()
        {
            base.Awake();

            Debug.Assert(resumeButton != null, "resumeButton can't be null!");
            Debug.Assert(leaveButton != null, "leaveButton can't be null!");

            resumeButton.onClick.AddListener(Hide);
            leaveButton.onClick.AddListener(() => { LevelManager.LoadLevel(LevelManager.Type.MainMenu); });

            Hide();
        }
    }
}
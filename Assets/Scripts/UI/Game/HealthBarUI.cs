﻿using UnityEngine;

namespace UI.Game
{
    public class HealthBarUI : MonoBehaviour
    {
        [SerializeField] private Transform healthPointTemplate;
        [SerializeField] private Transform healthContainer;

        private void Awake()
        {
            Debug.Assert(healthPointTemplate != null, "healthPointTemplate can't be null!");
            Debug.Assert(healthContainer != null, "healthContainer can't be null!");

            healthPointTemplate.gameObject.SetActive(false);
        }

        public void SetHealthPoint(int amount)
        {
            //TODO: Object pooing

            //CLEAR
            var healthObjCount = healthContainer.childCount;
            for (var i = 0; i < healthObjCount; i++)
            {
                var healthObj = healthContainer.GetChild(i);
                Destroy(healthObj.gameObject);
            }

            //ADD HEALTH ELEMENT
            for (var i = 0; i < amount; i++)
            {
                var healthObj = Instantiate(healthPointTemplate, healthContainer);
                healthObj.gameObject.SetActive(true);
            }
        }
    }
}
﻿using Player;
using UnityEngine;

namespace Item
{
    public class MagazineItem : MonoBehaviour
    {
        [SerializeField] private int ammo;
        private Magazine _magazine;

        private void Awake()
        {
            _magazine = new Magazine(ammo);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent<PlayerCharacter>(out var player)) return;
            player.PickUp(_magazine);
            Destroy(gameObject);
        }
    }
}
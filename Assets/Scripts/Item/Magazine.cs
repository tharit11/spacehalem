﻿namespace Item
{
    public class Magazine
    {
        public Magazine(int ammo)
        {
            this.ammo = ammo;
        }

        public int ammo { get; private set; }

        public bool HasAmmo => ammo > 0;

        public void Take()
        {
            ammo--;
        }
    }
}
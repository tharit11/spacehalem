﻿using System;

namespace Item
{
    public class Gun
    {
        private Magazine _magazine;

        public Gun(int damage, int ammo)
        {
            Damage = damage;
            _magazine = new Magazine(ammo);
        }

        public int Damage { get; }
        public int Ammo => _magazine.ammo;

        public event Action OnShoot;
        public event Action<int> OnAmmoChanged;

        public void Shoot()
        {
            if (!_magazine.HasAmmo) return;

            _magazine.Take();
            OnShoot?.Invoke();
            OnAmmoChanged?.Invoke(_magazine.ammo);
        }

        public void Reload(Magazine magazine)
        {
            _magazine = magazine;
            OnAmmoChanged?.Invoke(_magazine.ammo);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Item;
using UnityEngine;

namespace Player
{
    public class PlayerCharacter : Character
    {
        [Header("GUN")] [SerializeField] private int gunDamage;
        [SerializeField] private int initAmmo;
        [SerializeField] private float shootDegree;
        [SerializeField] private float shootDistance;
        [Header("PORTAL")] [SerializeField] private int initPortalIndex;

        public readonly Stack<Magazine> Magazines = new Stack<Magazine>();
        public Gun Gun { get; private set; }
        public int Score { get; private set; }
        public int Level { get; private set; }
        public int PortalIndex { get; private set; }

        public float ShootDistance => shootDistance;
        public float ShootDegree => shootDegree;

        private void Awake()
        {
            Gun = new Gun(gunDamage, initAmmo);
            PortalIndex = initPortalIndex;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space)) Gun.Shoot();

            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (Magazines.Count < 1) return;

                Gun.Reload(Magazines.Pop());
                OnMagazineCountChanged?.Invoke(Magazines.Count);
            }
        }

        public event Action<int> OnMagazineCountChanged;
        public event Action<int, int> OnScoreChanged;
        public event Action<int> OnLevelChanged;

        public void PickUp(Magazine magazine)
        {
            Magazines.Push(magazine);
            OnMagazineCountChanged?.Invoke(Magazines.Count);
        }

        public void AddScore(int amount)
        {
            var previousScore = Score;
            Score += amount;
            OnScoreChanged?.Invoke(previousScore, Score);
        }

        public void SetPortalIndex(int portalIndex)
        {
            switch (portalIndex)
            {
                case 0:
                    PortalIndex = 2;
                    break;
                case 1:
                    PortalIndex = 3;
                    break;
                case 2:
                    PortalIndex = 0;
                    break;
                default:
                    PortalIndex = 1;
                    break;
            }
        }

        public void NextLevel()
        {
            Level++;
            OnLevelChanged?.Invoke(Level);
        }
    }
}
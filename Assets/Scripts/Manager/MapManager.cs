﻿using System.Collections.Generic;
using Enemy;
using Item;
using Player;
using UnityEngine;

namespace Manager
{
    public class MapManager : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer mapSpriteRenderer;
        [SerializeField] private Map[] mapTop;
        [SerializeField] private Map[] mapRight;
        [SerializeField] private Map[] mapBottom;
        [SerializeField] private Map[] mapLeft;

        [Header("ENEMY SPAWNER")] [SerializeField]
        private int easyEnemyCount;

        [SerializeField] private int normalEnemyCount;
        [SerializeField] private int difficultEnemyCount;
        [SerializeField] private int hardEnemyCount;

        [Header("CHARACTER PREFAB")] [SerializeField]
        private EnemyCharacter enemyPrefab;

        [Header("MAP DECORATION")] [SerializeField]
        private int minDecoration;

        [SerializeField] private int maxDecoration;

        [SerializeField] private GameObject smallTreePrefab;

        [SerializeField] private GameObject bigTreePrefab;
        [SerializeField] private GameObject brushTreePrefab;
        [SerializeField] private GameObject rockTreePrefab;

        [Header("CARTRIDGE")] [SerializeField] private int minCartridgeSpawn;
        [SerializeField] private int maxCartridgeSpawn;
        [Space] [SerializeField] private float cartridgeSpawnRadius;
        [SerializeField] private MagazineItem cartridgePrefab;

        [SerializeField] private Transform mapContainer;
        [SerializeField] private Transform[] spawnTransform;

        [Tooltip("Portal: Top, Right, Bottom, Left")] [SerializeField]
        private Portal[] portals;

        private List<Transform> _availableSpawnPoint;

        public IEnumerable<Portal> Portals => portals;

        private void Awake()
        {
            Debug.Assert(mapSpriteRenderer != null, "mapSpriteRenderer can't be null!");
            Debug.Assert(mapTop != null, "mapTop can't be null!");
            Debug.Assert(mapRight != null, "mapRight can't be null!");
            Debug.Assert(mapBottom != null, "mapBottom can't be null!");
            Debug.Assert(mapLeft != null, "mapLeft can't be null!");
            Debug.Assert(bigTreePrefab != null, "bigTreePrefab can't be null!");
            Debug.Assert(smallTreePrefab != null, "smallTreePrefab can't be null!");
            Debug.Assert(brushTreePrefab != null, "brushTreePrefab can't be null!");
            Debug.Assert(rockTreePrefab != null, "rockTreePrefab can't be null!");
            Debug.Assert(cartridgePrefab != null, "cartridgePrefab can't be null!");
            Debug.Assert(mapContainer != null, "mapContainer can't be null!");
            Debug.Assert(spawnTransform != null, "spawnTransform can't be null!");
            Debug.Assert(portals != null, "portals can't be null!");
            Debug.Assert(portals.Length == 4, "must have 4 portals");
        }


        public BuildResultData BuildLevel(PlayerCharacter playerCharacter)
        {
            var buildLevelData = new BuildLevelData
            {
                Enemy = GetEnemyCountByLevel(playerCharacter.Level),
                Cartridge = Random.Range(minCartridgeSpawn, maxCartridgeSpawn),
                EnteredPortalIndex = playerCharacter.PortalIndex,
                Player = playerCharacter
            };

            //MAP SPRITE
            var map = buildLevelData.EnteredPortalIndex switch
            {
                0 => GetRandomMapSprite(mapTop),
                1 => GetRandomMapSprite(mapRight),
                2 => GetRandomMapSprite(mapBottom),
                _ => GetRandomMapSprite(mapLeft)
            };

            mapSpriteRenderer.sprite = map.MapSprite;

            //PORTAL
            portals[0].GetComponent<Collider>().isTrigger = map.PortalTop;
            portals[1].GetComponent<Collider>().isTrigger = map.PortalRight;
            portals[2].GetComponent<Collider>().isTrigger = map.PortalBottom;
            portals[3].GetComponent<Collider>().isTrigger = map.PortalLeft;

            var currentPortal = portals[buildLevelData.EnteredPortalIndex];
            currentPortal.GetComponent<Collider>().isTrigger = false;
            buildLevelData.Player.SetPosition(currentPortal.SpawnPoint.position);
            buildLevelData.Player.transform.rotation = currentPortal.SpawnPoint.rotation;

            //CLEAR MAP ELEMENTS
            foreach (Transform child in mapContainer) Destroy(child.gameObject);

            //SPAWN NEW ELEMENTS
            var enemies = new EnemyCharacter[buildLevelData.Enemy];
            _availableSpawnPoint = new List<Transform>(spawnTransform);
            for (var i = 0; i < buildLevelData.Enemy; i++)
            {
                if (_availableSpawnPoint.Count == 0) break;

                var treeObj = Instantiate(Random.Range(0, 100) > 50 ? smallTreePrefab : bigTreePrefab, mapContainer);
                var index = Random.Range(0, _availableSpawnPoint.Count);
                var spawnPoint = _availableSpawnPoint[index];
                _availableSpawnPoint.RemoveAt(index);
                var position = spawnPoint.position;
                treeObj.transform.position = position;

                var enemy = Instantiate(enemyPrefab, mapContainer);
                enemy.SetPosition(position);
                enemies[i] = enemy;
            }

            //SPAWN CARTRIDGE
            for (var i = 0; i < buildLevelData.Cartridge; i++)
            {
                var cartridge = Instantiate(cartridgePrefab, mapContainer);
                Transform cartridgeTransform;
                (cartridgeTransform = cartridge.transform).rotation = Quaternion.Euler(0, Random.Range(0f, 180f), 0);
                var randPosition = new Vector3(Random.insideUnitCircle.x * cartridgeSpawnRadius, 0,
                    Random.insideUnitCircle.y * cartridgeSpawnRadius);
                cartridgeTransform.position = randPosition;
            }

            //MAP DECORATION
            for (var i = 0; i < Random.Range(minDecoration, maxDecoration); i++)
            {
                if (_availableSpawnPoint.Count == 0) break;

                GameObject randElement;
                var randChance = Random.Range(0, 100);
                if (randChance > 80)
                    randElement = Instantiate(bigTreePrefab, mapContainer);
                else if (randChance > 70)
                    randElement = Instantiate(smallTreePrefab, mapContainer);
                else if (randChance > 30)
                    randElement = Instantiate(brushTreePrefab, mapContainer);
                else
                    randElement = Instantiate(rockTreePrefab, mapContainer);

                var index = Random.Range(0, _availableSpawnPoint.Count);
                var spawnPoint = _availableSpawnPoint[index];
                _availableSpawnPoint.RemoveAt(index);
                var position = spawnPoint.position;
                randElement.transform.position = position;
            }

            return new BuildResultData
            {
                Enemies = enemies,
                Cartridge = buildLevelData.Cartridge
            };
        }

        private Map GetRandomMapSprite(IReadOnlyList<Map> mapSprites)
        {
            return mapSprites[Random.Range(0, mapSprites.Count)];
        }

        private int GetEnemyCountByLevel(int level)
        {
            switch (level)
            {
                case 0:
                case 1:
                case 2:
                    return easyEnemyCount; //Easy
                case 3:
                case 4:
                case 5:
                case 6:
                    return normalEnemyCount; //Normal
                case 7:
                case 8:
                case 9:
                case 10:
                    return difficultEnemyCount; //Difficult
                default:
                    return hardEnemyCount; //Hard
            }
        }

        private struct BuildLevelData
        {
            public int Enemy;
            public int Cartridge;
            public int EnteredPortalIndex;
            public PlayerCharacter Player;
        }

        public struct BuildResultData
        {
            public EnemyCharacter[] Enemies;
            public int Cartridge;
        }
    }
}
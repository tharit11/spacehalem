﻿using UI;
using UI.MainMenu;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class MainMenuManager : MonoBehaviour
    {
        [Header("UI BUTTON")] [SerializeField] private Button playButton;
        [SerializeField] private Button howToPlayButton;
        [SerializeField] private Button creditsButton;
        [SerializeField] private Button quitButton;

        [Header("ELEMENT UI")] [SerializeField]
        private HowToPlayUI howToPlayUI;

        [SerializeField] private CreditsUI creditsUI;

        private void Awake()
        {
            FadeScreen.FadeIn(1);
            Debug.Assert(playButton != null, "playButton can't be null!");
            Debug.Assert(howToPlayButton != null, "howToPlayButton can't be null!");
            Debug.Assert(creditsButton != null, "creditsButton can't be null!");
            Debug.Assert(quitButton != null, "quitButton can't be null!");
            Debug.Assert(howToPlayUI != null, "howToPlayUI can't be null!");

            playButton.onClick.AddListener(Play);
            howToPlayButton.onClick.AddListener(HowToPlay);
            creditsButton.onClick.AddListener(Credits);
            quitButton.onClick.AddListener(Quit);
        }

        private void Play()
        {
            LevelManager.LoadLevel(LevelManager.Type.Game);
        }

        private void HowToPlay()
        {
            howToPlayUI.Show();
        }

        private void Credits()
        {
            creditsUI.Show();
        }

        private void Quit()
        {
            Application.Quit();
        }
    }
}
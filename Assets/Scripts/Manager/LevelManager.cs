using UnityEngine.SceneManagement;

namespace Manager
{
    public class LevelManager : Singleton<LevelManager>
    {
        public enum Type
        {
            SplashScreen = 0,
            MainMenu = 1,
            Game = 2
        }

        public static void LoadLevel(Type levelType)
        {
            SceneManager.LoadScene((int) levelType);
        }
    }
}
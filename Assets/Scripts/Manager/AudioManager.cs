﻿using UnityEngine;

namespace Manager
{
    public class AudioManager : MonoBehaviour
    {
        [Header("BACKGROUND LOOP")] public AudioClip ambienceLoop;
        public AudioClip mainMenuLoop;

        [Header("ENEMY")] public AudioClip enemyDead;

        [Header("PLAYER")] public AudioClip playerHit;

        [Header("GUN")] public AudioClip shoot;
        public AudioClip reload;

        private void Awake()
        {
            Debug.Assert(ambienceLoop != null, "ambienceLoop can't be null!");
            Debug.Assert(mainMenuLoop != null, "mainMenuLoop can't be null!");

            Debug.Assert(enemyDead != null, "enemyDead can't be null!");

            Debug.Assert(playerHit != null, "playerHit can't be null!");

            Debug.Assert(shoot != null, "shoot can't be null!");
            Debug.Assert(reload != null, "reload can't be null!");
        }

        //TODO

        public void Play(AudioClip audioClip)
        {
            Debug.Assert(audioClip, "Audio clip can't be null!");

            if (!audioClip)
            {
                return;
            }

            Debug.Log("PLAY!");

            LeanAudio.play(audioClip);
        }
    }
}
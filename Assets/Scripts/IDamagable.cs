﻿using UnityEngine.UI;

public interface IDamagable
{
    void TakeDamage(int damage);
}
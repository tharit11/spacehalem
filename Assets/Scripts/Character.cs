﻿using System;
using UnityEngine;

public abstract class Character : MonoBehaviour, IVisible, IDamagable
{
    [SerializeField] private int health;
    public int Health => health;
    public bool IsDead => health < 1;
    public Vector3 SpawnPosition { get; private set; }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public event Action<int, int> OnHealthChanged;
    public event Action<Character> OnDeath;

    public virtual void TakeDamage(int amount)
    {
        SetHealth(Health - amount);
    }

    public virtual void SetHealth(int amount)
    {
        var previousHealth = Health;
        health = amount;
        OnHealthChanged?.Invoke(previousHealth, Health);

        if (Health > 0) return;

        Dead();
        OnDeath?.Invoke(this);
    }

    protected virtual void Dead()
    {
    }

    public void Respawn()
    {
        transform.position = SpawnPosition;
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
        SpawnPosition = position;
    }
}
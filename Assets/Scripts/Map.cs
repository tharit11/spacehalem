﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Map", menuName = "Map", order = 0)]
public class Map : ScriptableObject
{
    [SerializeField] private Sprite mapSprite;
    [SerializeField] private bool portalTop;
    [SerializeField] private bool portalRight;
    [SerializeField] private bool portalBottom;
    [SerializeField] private bool portalLeft;

    public Sprite MapSprite => mapSprite;
    public bool PortalTop => portalTop;
    public bool PortalRight => portalRight;
    public bool PortalBottom => portalBottom;
    public bool PortalLeft => portalLeft;
}